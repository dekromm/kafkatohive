name := "spark_streaming"

scalaVersion := "2.11.9"

mainClass in Compile := Some("sia.spark_streaming.SparkConsumer")

libraryDependencies ++= Seq("org.apache.spark" % "spark-streaming_2.11" % "2.1.1",
"org.apache.spark" % "spark-streaming-kafka-0-8_2.11" % "2.1.1",
"org.apache.spark" % "spark-sql_2.11" % "2.1.1",
"com.databricks" % "spark-avro_2.10" % "3.2.0",
"org.apache.hive" % "hive-jdbc" % "1.1.0",
"org.apache.avro" % "avro" % "1.8.1",
"com.twitter" % "bijection-core_2.10" % "0.9.5",
"com.twitter" % "bijection-avro_2.10" % "0.9.5",
"com.typesafe" % "config" % "1.2.1",
"org.apache.parquet" % "parquet-avro" % "1.9.0"
)