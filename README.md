# kafkatohive #

kafkatohive is a software component that 

 1. reads Avro records from a Kafka queue
 2. converts the data into Parquet files 
 3. stores them in Hive's HDFS path
 
following the schema and partitioning specified by Apache Spot for DNS traffic.

## Usage

The project is meant to be built as a jar package.
In order for the jar to work it is required that you put in the current folder at least DnsRecord.avsc (which is the Avro schema of the records read from Kafka queue).
By deafault, with no arguments, the jar will also look for app.conf; you can also specify your configuration file

    #Example with no arguments: DnsRecord.avsc and app.conf must be in . folder
    java -jar $jarfolder/kafkatohive.jar

    #Example with configuration argument: DnsRecord.avsc must be in . folder
    java -jar $jarfolder/kafkatohive.jar $conffolder/custom.conf

## Building

With IntelliJ, open the scala project and build the artifact specifying src/main/conf as resource folder.