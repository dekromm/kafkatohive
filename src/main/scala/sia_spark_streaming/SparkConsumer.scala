package sia_spark_streaming

import java.io.{File, FileInputStream, IOException, InputStream}
import java.net.URI
import java.sql.{Connection, DriverManager, Statement}
import java.text.{DateFormat, SimpleDateFormat}
import java.util
import java.util._

import com.twitter.bijection.Injection
import com.twitter.bijection.avro.GenericAvroCodecs
import kafka.common.TopicAndPartition
import kafka.message.MessageAndMetadata
import kafka.serializer.{DefaultDecoder, StringDecoder}
import org.apache.avro.Schema
import org.apache.avro.generic.GenericRecord
import org.apache.commons.io.IOUtils
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.util.Time
import org.apache.kafka.common.TopicPartition
import org.apache.log4j.{Level, LogManager, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaPairRDD
import org.apache.spark.api.java.function.{Function0, VoidFunction}
import org.apache.spark.sql.{Dataset, Row, SaveMode, SparkSession}
import org.apache.spark.streaming.{Duration, Seconds, StreamingContext}
import org.apache.spark.streaming.kafka.KafkaUtils
import org.apache.spark.streaming.kafka._
import org.apache.spark.api.java.function.{Function0 => JFunction0}
import org.apache.spark.streaming.api.java.{JavaPairInputDStream, JavaStreamingContext}
import org.apache.spark.streaming.dstream.InputDStream

import scala.io.Source

object SparkConsumer extends Serializable {

  private var appConfigFile: String = "./app.conf"
  private var master: String = _
  private var zkQuorum: String = _
  private var group: String = _
  private var topicName: String = _
  private var numThreads: String = _
  private var kafkaAddr: String = _
  private var hdfsAddr: String = _
  private var checkpointPath: String = _
  private var hdfsPath: String = _
  private var hiveAddr: String = _
  private var dbName: String = _
  private var tableName: String = _
  private var testFeatures: String = _
  private var fromQueueBeginning: String = _
  private var last_h: String = "not_initialized"
  private var current_h: String = _
  private val hiveDriverName: String = "org.apache.hive.jdbc.HiveDriver"
  private var schema: Schema = _
  private val log = LogManager.getLogger(getClass)

  val parser: Schema.Parser = new Schema.Parser()

  def main(args: Array[String]): Unit = {

    log.setLevel(Level.ALL)

    log.info("kafkaToHive Started!")

    try schema = parser.parse(new File("./DnsRecord.avsc"))
    catch {
      case e: IOException =>
        log.error("Can't find schema DnsRecord.avsc")
        System.exit(1)
    }

    if (args.length == 1)
      appConfigFile = args(0)
    //Load configuration file
    readConfig()
    //Hive driver test
    try
      Class.forName(hiveDriverName)
    catch {
      case e: ClassNotFoundException => {
        e.printStackTrace()
        System.exit(1)
      }

    }
    //needed to load winutils.exe and write parquet files
    System.setProperty("hadoop.home.dir", System.getProperty("user.dir"))
    System.setProperty("user.name", "hdfs")
    System.setProperty("HADOOP_USER_NAME", "hdfs")

    // val createContextFunc:  Function0[JavaStreamingContext] = () => createContext(checkpointPath)

    val jssc = createContext(checkpointPath)

    jssc.start()
    jssc.awaitTermination()
  }


  private def createContext(checkpointDirectory: String): JavaStreamingContext = {

    val conf: SparkConf = new SparkConf().setAppName("EnrichStreamingData").setMaster(master)
    conf.set("mapreduce.fileoutputcommitter.marksuccessfuljobs", "false")

    val jssc = new JavaStreamingContext(conf, Seconds(10))
    val topicMap: HashMap[String, Integer] = new HashMap[String, Integer]()
    topicMap.put(topicName, java.lang.Integer.parseInt(numThreads))

    val spark: SparkSession = SparkSession.builder().appName("Parquetter").getOrCreate
    val topicSet = topicMap.keySet

    val kafkaParams: Map[String, String] = new HashMap[String, String]()
    kafkaParams.put("metadata.broker.list", kafkaAddr)
    //Start from first event in the queue
    if (fromQueueBeginning.equals("true")) {
      kafkaParams.put("auto.offset.reset", "smallest")
    }
    //Manage manually your offset
    //kafkaParams.put("auto.offset.reset", "none")


    val RDDHandlerFunctionAvro: VoidFunction[JavaPairRDD[String, Array[Byte]]] =

      new VoidFunction[JavaPairRDD[String, Array[Byte]]]() {

        override def call(rdd: JavaPairRDD[String, Array[Byte]]): Unit = {
          log.info("--- New RDD with " + rdd.partitions.size + " partitions and " + rdd.count() + " records")
          if (rdd != null) {
            if (rdd.count() > 0) {
              val recordInjection: Injection[GenericRecord, Array[Byte]] = GenericAvroCodecs.apply(schema)

              val result = rdd.collect.iterator
              var processingTime: java.lang.Long = Time.now()
              val kafkaMessages: List[DnsRecord] = new ArrayList[DnsRecord]()

              while (result.hasNext) {
                val line = result.next._2
                try {
                  val record: GenericRecord = recordInjection.invert(line).get
                  log.info("got record: " + recordInjection.invert(line).get)
                  kafkaMessages.add(new DnsRecord(record))
                } catch {
                  case e: Exception => log.error("Record malformed: " + line)
                }
              }

              val df: Dataset[Row] = spark.createDataFrame(kafkaMessages, classOf[DnsRecord])
              val dfor: DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
              val str: String = dfor.format(Time.now())
              val filename: String = "y=" + str.substring(6, 10) + "/m=" + str
                .substring(3, 5) +
                "/d=" +
                str.substring(0, 2) +
                "/h=" +
                str.substring(11, 13)

              df.write
                .mode(SaveMode.Append)
                .option("compression", "none")
                .parquet("hdfs://" + hdfsAddr + hdfsPath + filename)

              //Count partition-specific occurrences, if newly created force hive to recognize it
              current_h = str.substring(11, 13)
              if (last_h != current_h) {
                //query hive
                val con: Connection = DriverManager.getConnection("jdbc:hive2://" + hiveAddr + "/" + dbName, "hive", "")
                val stmt: Statement = con.createStatement()
                stmt.execute("MSCK REPAIR TABLE " + tableName)
                log.info("Switching from partition h=" + last_h + " to h=" + current_h + ", poked hive to make it sense changes")
                last_h = current_h
              }
              processingTime = Time.now() - processingTime
              log.info("File saved. Processing time: " + processingTime)
            }
          }
        }
      }

    val RDDHandlerFunctionTest: VoidFunction[JavaPairRDD[String, Array[Byte]]] =
      new VoidFunction[JavaPairRDD[String, Array[Byte]]]() {

        override def call(rdd: JavaPairRDD[String, Array[Byte]]): Unit = {
          log.info("--- New RDD with " + rdd.partitions.size + " partitions and " + rdd.count() + " records")
          val result = rdd.collect.iterator
          while (result.hasNext) {
            val recordInjection: Injection[GenericRecord, Array[Byte]] = GenericAvroCodecs.apply(schema)
            val record: GenericRecord = recordInjection.invert(result.next._2).get
            val r: DnsRecord = new DnsRecord(record)
            log.info("printed: " + r.testPrint())
          }
        }
      }

    testFeatures.toUpperCase() match {
      case "FALSE" => {
        kafkaParams.put("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")
        kafkaParams.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")
        val directKafkaStream: JavaPairInputDStream[String, Array[Byte]] =
          KafkaUtils.createDirectStream(jssc,
            classOf[String], classOf[Array[Byte]],
            classOf[StringDecoder], classOf[DefaultDecoder],
            kafkaParams, topicSet)

        directKafkaStream.foreachRDD(RDDHandlerFunctionAvro)
        //break
      }
      case "TRUE" => {
        kafkaParams.put("key.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")
        kafkaParams.put("value.deserializer", "org.apache.kafka.common.serialization.ByteArrayDeserializer")

        val directKafkaStream: JavaPairInputDStream[String, Array[Byte]] =
          KafkaUtils.createDirectStream(jssc,
            classOf[String], classOf[Array[Byte]],
            classOf[StringDecoder], classOf[DefaultDecoder],
            kafkaParams, topicSet)

        directKafkaStream.foreachRDD(RDDHandlerFunctionTest)
        //break
      }
      case _ => {
        log.error("incorrect testFeatures set, only \"true\" and \"false\" are allowed")
        System.exit(1)
        //break
      }

    }

    jssc
  }

  private def readConfig(): Unit = {
    val prop = new Properties()
    try if (validateProp(appConfigFile)) {
      val is = new FileInputStream(appConfigFile)
      prop.load(is)
      if (prop.size == 15) {
        val arguments = new util.HashSet[String](
          util.Arrays.asList("master", "zkQuorum", "group", "topicName", "numThreads", "kafkaAddr", "fromQueueBeginning", "hdfsAddr",
            "checkpointPath", "hdfsPath", "hiveAddr", "dbName", "tableName", "recordType", "testFeatures"))

        if (new util.HashSet(Collections.list(prop.keys)) == arguments) {
          master = prop.getProperty("master")
          zkQuorum = prop.getProperty("zkQuorum")
          group = prop.getProperty("group")
          topicName = prop.getProperty("topicName")
          numThreads = prop.getProperty("numThreads")
          kafkaAddr = prop.getProperty("kafkaAddr")
          hdfsAddr = prop.getProperty("hdfsAddr")
          checkpointPath = prop.getProperty("checkpointPath")
          hdfsPath = prop.getProperty("hdfsPath")
          hiveAddr = prop.getProperty("hiveAddr")
          dbName = prop.getProperty("dbName")
          tableName = prop.getProperty("tableName")
          testFeatures = prop.getProperty("testFeatures")
          fromQueueBeginning = prop.getProperty("fromQueueBeginning")
        } else {
          log.error("incorrect config parameters spelling! Should be: master, zkQuorum, group, topicName, numThreads, kafkaAddr, hdfsAddr, hdfsPath, hiveAddr, dbName, tableName")
          System.exit(1)
        }
      } else {
        log.error("incorrect number of config parameters!")
        System.exit(1)
      }
    } else {
      log.error("one or more lines doesn't follow the schema KEY=VALUE")
      System.exit(1)
    } catch {
      case e: IOException =>
        log.error("Can't find config file " + appConfigFile)
        System.exit(1)
    }
  }

  private def validateProp(res: String): Boolean = {
    val propString = IOUtils.toString(new FileInputStream(res), "UTF8")
    if (!propString.matches("^(\\w+\\s*=\\s*\\S+\\r?\\n)*(\\w+\\s*=\\s*\\S+)\\r*?\\n*?$")) false
    true
  }

}
