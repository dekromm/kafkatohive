package sia_spark_streaming

import org.apache.avro.generic.GenericRecord

/**
  * Created by jacopo.russo on 07/06/2017.
  */
class DnsRecord {

  private var frame_time: String  = ""
  var unix_tstamp: Long = 0L
  var frame_len: Int = 0
  var ip_dst: String = ""
  var ip_src: String = ""
  var dns_qry_name: String = ""
  var dns_qry_class: String = ""
  var dns_qry_type: Int = 0
  var dns_qry_rcode: Int = 0
  var dns_a: String = ""


  def this(csv: String){
    this()
    val csvArray:Array[String] = csv.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)")
    frame_time = csvArray(0)
    unix_tstamp = csvArray(1).toLong
    frame_len = csvArray(2).toInt
    ip_dst = csvArray(3)
    ip_src = csvArray(4)
    dns_qry_name = csvArray(5)
    dns_qry_class = csvArray(6)
    dns_qry_type = csvArray(7).toInt
    dns_qry_rcode = csvArray(8).toInt
    dns_a = csvArray(9)
  }

  def this(record: GenericRecord) {
    this()
    frame_time = if (record.get("frame_time") != null) record.get("frame_time").toString
    else "\"\""
    unix_tstamp = if (record.get("unix_tstamp") != null) record.get("unix_tstamp").asInstanceOf[Long]
    else 0
    frame_len = if (record.get("frame_len") != null) record.get("frame_len").asInstanceOf[Int]
    else 0
    ip_dst = if (record.get("ip_dst") != null) record.get("ip_dst").toString
    else "\"\""
    ip_src = if (record.get("ip_src") != null) record.get("ip_src").toString
    else "\"\""
    dns_qry_name = if (record.get("dns_qry_name") != null) record.get("dns_qry_name").toString
    else "\"\""
    dns_qry_class = if (record.get("dns_qry_class") != null) record.get("dns_qry_class").toString
    else "\"\""
    dns_qry_type = if (record.get("dns_qry_type") != null) record.get("dns_qry_type").asInstanceOf[Int]
    else 0
    dns_qry_rcode = if (record.get("dns_qry_rcode") != null) record.get("dns_qry_rcode").asInstanceOf[Int]
    else 0
    dns_a = if (record.get("dns_a") != null) record.get("dns_a").toString
    else "\"\""
  }

  def testPrint(): String = {getFrame_time + getUnix_tstamp + getFrame_len + getIp_dst + getIp_src + getDns_qry_name + getDns_qry_class + getDns_qry_type + getDns_qry_rcode + getDns_a}

  def getFrame_time: String = frame_time

  def setFrame_time(frame_time: String): Unit = {
    this.frame_time = frame_time
  }

  def getUnix_tstamp: Long = unix_tstamp

  def setUnix_tstamp(unix_tstamp: Long): Unit = {
    this.unix_tstamp = unix_tstamp
  }

  def getFrame_len: Integer = frame_len

  def setFrame_len(frame_len: Integer): Unit = {
    this.frame_len = frame_len
  }

  def getIp_dst: String = ip_dst

  def setIp_dst(ip_dst: String): Unit = {
    this.ip_dst = ip_dst
  }

  def getIp_src: String = ip_src

  def setIp_src(ip_src: String): Unit = {
    this.ip_src = ip_src
  }

  def getDns_qry_name: String = dns_qry_name

  def setDns_qry_name(dns_qry_name: String): Unit = {
    this.dns_qry_name = dns_qry_name
  }

  def getDns_qry_class: String = dns_qry_class

  def setDns_qry_class(dns_qry_class: String): Unit = {
    this.dns_qry_class = dns_qry_class
  }

  def getDns_qry_type: Integer = dns_qry_type

  def setDns_qry_type(dns_qry_type: Integer): Unit = {
    this.dns_qry_type = dns_qry_type
  }

  def getDns_qry_rcode: Integer = dns_qry_rcode

  def setDns_qry_rcode(dns_qry_rcode: Integer): Unit = {
    this.dns_qry_rcode = dns_qry_rcode
  }

  def getDns_a: String = dns_a

  def setDns_a(dns_a: String): Unit = {
    this.dns_a = dns_a
  }
}
